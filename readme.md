# CosmOS Documentation

Documentation and Screenshots

## Documentation that is still TODO
- Contributor Guide
- Bootloader
- Memory Map Layout
- ABI (Application Binary Interface)
    - System Calls
    - Driver Registration
    - Driver ABI
- Design of Modularity
- Needed Modules

## Setting up the workspace
First create a folder for your workspace. You can call it however you
want, but probably "cosmos" (or "CosmOS") is the best option.

After that, clone the following repositories:
- [kernel](https://codeberg.org/cosmos/kernel)
- [binaries](https://codeberg.org/cosmos/binaries)
- [bootloader](https://codeberg.org/cosmos/bootloader)

`cd` into the _bootloader/gnu-efi_ directory and build the bootloader:
```shell
cd bootloader/gnu-efi
make
```

then `cd` into the _kernel_ directory and run the setup Makefile:
```shell
make setup
```

## Build & Run

### Overview
All build files are written to the automatcally created _build_ directory.

Steps (For first-time compilation):
- Bootloader
    The bootloader loads the kernel with the RAM-Disk into the RAM and
    executes it after that.
- RAM-Disk
    The RAM-Disk contains the initial modules that are needed for the
    kernel to be able to read a file from the disk and load other modules.
- Kernel
    The kernel is the central piece of the operating system. In CosmOS, it
    is tried to keep this part relatively small.



### Bootloader
**Working Directory**: _bootloader/gnu-efi_

Now build the Bootloader using the following command:
```shell
make bootloader
```



### RAM-Disk
**Working Directory**: _kernel_

To pack the content of _binaries/ramdisk_ into the ramdisk
archive use the following command:
```shell
make ramdisk
```



### Kernel
**Working Directory**: _kernel_

Compile the kernel using the following commands:
```shell
make kernel
make buildimg
```
*In case of failure:*
Sometimes there is unexpected behaviour, when headers are changed but
source files, that include these headers are not re-compiled.

Do the following to do a clean build before filing an issue: 
```shell
make clean
make setup
make ramdisk
```

This will make sure that ALL files are compiled again. This has the
drawback of a longer compile-time, but then, it works.



### Run
**Working Directory**: _kernel_

To run the image use this command:
```shell
make run
```

---

## Documentation

The, at the moment not existing, bootloader documentation will be located at:
_documentation/bootloader_

The kernel is documented at: _documentation/kernel_

