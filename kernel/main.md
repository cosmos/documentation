# Kernel Documentation


## Printing to the screen
To print to the screen the kernel needs to be initialized and this library need to be included:
* **graphics/Renderer.h**

### printf
Printf formats a string and prints it out (to the global framebuffer).

**Arguments**:
* char* - string to print
* Formatting data

**Format options**:
Place a format option in your string to identify a format parameter and its type.
* ```%s``` - insert the parameter as string
* ```%c``` - insert the parameter as character
* ```%d``` - insert the parameter as decimal number
* ```%f``` - insert the parameter as float
* ```%?``` - insert the parameter as boolean
* ```%b``` - insert the parameter as binary
* ```%x``` - insert the parameter as hexadecimal number
    * ```%x8```  - insert as 8-Bit hexadecimal number
    * ```%x16``` - insert as 16-Bit hexadecimal number
    * ```%x32``` - insert as 32-Bit hexadecimal number
    * ```%x64``` - insert as 64-Bit hexadecimal number
* ```%C``` -  set the color until the next %C (only for this printf).

**Note about ```%C```:**
The ```%C``` parameters need to be passed after all other parameters.

**Example:**

Code:
```c
printf("Hello cosmos - this is a number: %d", 1312);
```

Output:
```
Hello cosmos - this is a number: 1312
```

---

# Writing logs

To write logs this library need to be included:
* **utils/Logger.h**

All logs are written to _kernel/serial.log_
and some types of log are printed onto the screen.

There are a few **types** of log:
* **LOGGING_SUCCESS** - Print to Screen: yes
* **LOGGING_INFO** - Print to Screen: no
* **LOGGING_DEBUG** - Print to Screen: yes
* **LOGGING_WARNING** - Print to Screen: yes
* **LOGGING_ERROR** - Print to Screen: yes
* **LOGGING_NONE** - Print to Screen: no
* **LOGGING_NONE_PRINT** - Print to Screen: yes


**Usage of ```log```:**

Parameters: 
* type - type of log
* string - string to log
* format data - data fo formatting

The formatting is the same as in ```printf```, just without ```%C```.

**Example:**
```c
log(LOGGING_DEBUG, "Test -> This is a debug message with a number: %d", 1312);
```

Output:
```
[  DEBUG  ] Test -> This is a debug message with a number: 1312
```

---

# Kernel Panic
When the kernel gets into trouble that cannot be recovered from,
a Kernel Panic is thrown.
This dumps information about the error and halts the system.
A stack backtrace to dump which function caused the panic is planned.

To set kernel panics use this library:
* **utils/Panic.h**

**Example (from _interrupts/Interrupts.c_):**

Code:
```c
panic("Divide by Zero Error", state);
```

Output:
```
[  ERROR  ] !=====[ KERNEL PANIC ]=====!
Exception: Divide by Zero Error
Interrupt: 0x00
Error Code: 0x0000000000000000

CPU EFLAGS:
    IF  - Interrupt Enable

CPU Registers:
    RAX: 0x000000000000002A  RBX: 0x000000000F9EE018  RCX: 0x0000000000000000
    RDX: 0x0000000000000000  RSI: 0x00000000FFB6B6B6  RDI: 0x000000000000000D
    RBP: 0x000000000FF07180  RSP: 0x000000000FF07180  RIP: 0x0000100000093521

Stack Backtrace (most recent call first):
    <Scope>        <Return Address>        <Function Name>
    Process 0   ->  0x0000100000093521  ->  _start
    Kernel      ->  0x0000000000002D87  ->  test_elf_execution
    Kernel      ->  0x0000000000002E1A  ->  _start

[ WARNING ] !=====[ SYSTEM HALTED ]=====!
```

---

# Allocating Memory
For dynamic memory allocation the kernel has a heap.
To access its functions include:
* **memory/Heap.h**

## malloc
 

## free

---

# Manage Memory

## memset

## memcmp

## memcpy

---

# I/O Library

---

# CPU information

---

# CPU states

---

# PCI Bus

---

# ACPI

---

# AHCI (just read at the moment)

---

# USB

---

# PIT (Programmable Interval Timer)

---

# Math Library

---

# String utils

---

# File management

---

# Interrupts

---

# Bitmaps

---

# Paging

---

# JSON parser

---

# ELF Executables

---

# Processes

---

# Process scheduling / Multi tasking