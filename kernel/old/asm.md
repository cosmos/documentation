The Assembly files are written in **Intel x86_64 Assembly** (nasm).
All the inline assembly needs to be ported into assembly files.

# Source files

---

## gdt.asm
### load_GDT
Loads a General Descriptor Table into the CPU and sets the segmantation registers.

C definition (in _kernel/GDT.h_):
```c
extern void load_GDT(GDTDescriptor_T* gdt_descriptor);
```