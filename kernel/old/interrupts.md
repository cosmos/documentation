# Source files

---

## IDT.c

### set_idt_descriptor_entry_offset

**Return Type:** void

**Parameters:**

| type                  | name    | explaination           |
|-----------------------|---------|------------------------|
| IDTDescriptorEntry_T* | entry   | entry to set offset in |
| uint64_t              | offset  | Offset to set          |

**Description:**

sets the given offset into the 3 offset variables of the given entry.

---
### get_idt_descriptor_entry_offset
**Return Type:** uint64_t

**Parameters:**

| type                  | name    | explaination              |
|-----------------------|---------|---------------------------|
| IDTDescriptorEntry_T* | entry   | entry to read offset from |

**Description:**

reads the offset of an IDT entry and returns it as uint64_t.

---
## Interrupts.c


# Header files

---

## Renderer.h
Header for **Renderer.c**.
### Functions:
```c
void        printf          (const char* str, ...);
void        put_pixel       (point_T coords, color_t clr);
color_t     get_pixel       (point_T coords);
void        scroll          ();
```

---

## Color.h
