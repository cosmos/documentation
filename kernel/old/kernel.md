# The CosmOS Kernel

The Kernel code located at the [Kernel Repo](https://codeberg.org/cosmos/kernel).

# Source Files

## Kernel.c
**Header:** _NONE_

### _start

**Return Type:** void

**Parameters:**

| type           | name      | explaination                |
|----------------|-----------|-----------------------------|
| boot_info_t*   | boot_info | explained at Bootloader Doc |

**Description:**

This is the Entry of the kernel and is called by the Bootloader.

**Tasks:**
  * Call the Kernel [Init function](#initialize_kernel)

---

## Init.c
**Header:** _inc/kernel/Kernel.h_

### initialize_kernel
**Return Type:** void

**Parameters:**

| type           | name      | explaination                |
|----------------|-----------|-----------------------------|
| boot_info_t*   | boot_info | explained at Bootloader Doc |

**Description:**

Initialize all the things the kernel needs.

**Tasks:**
* set global font and framebuffer variables
* Init GeneralDescriptorTable
* Initialize Paging
* Initialize Heap
* Initialize CPU Interrupts

---

## kernel/GDT.c
**Header:** _inc/kernel/GDT.h_

_GDT.c_ has no functions. it just defines a General Descriptor Table.

---