# Source files

---

## Renderer.c

### printf

**Return Type:** void

**Parameters:**

| type        | name   | explaination               |
|-------------|--------|----------------------------|
| const char* | string | String to format and Print |
| ...         | ...    | Variadic arguments         |

**Description:**

printf takes a string and parses it with the given variadic arguments.
The parsing identifiers are the usual (v)format identifiers plus **%C**.

**%C** specifies the Color until the next **%C**.
All of its arguments are behind all the other format arguments.

**Example call:**
```c
printf("%Cred text%C - dec %d - hex %x", 42, 42, 0xFFFF0000, 0xFFFFFFFF);
```
the output would be the following:
```
[  RED ][      WHITE     ]
red text - dec 42 - hex 2A
```

---
### put_pixel

**Return Type:** void

**Parameters:**

| type    | name   | explaination                           |
|---------|--------|----------------------------------------|
| point_T | coords | Position where the Pixel should be set |
| color_t | clr    | Color to set                           |

**Description:**

Sets a pixel at the given position in the given color.

**Example call:**
```c
put_pixel({50, 100}, 0xFFFFFFFF);
```
Sets a white pixel at X position 50 and Y position 100.

---
### get_pixel

**Return Type:** color_t

**Parameters:**

| type    | name   | explaination                |
|---------|--------|-----------------------------|
| point_T | coords | Position to read pixel from |

**Description:**

Returns the color of the given Pixel.

**Example call:**
```c
color_t clr = get_pixel({50, 100});
```
Writes the color of Pixel X position 50 and Y position 100 into clr;

---
### scroll

**Return Type:** void

**Parameters:** _NONE_

**Description:**

Moves the content on the Screen 1 line upwards.

---
### put_char

**Return Type:** void

**Parameters:**

| type    | name | explaination  |
|---------|------|---------------|
| char    | chr  | Char to print |
| color_t | clr  | color of char |

**Description:**

Prints the given char inn the given color at the cursor position.
Used by printf.

**Example call:**
```c
put_char('X', 0xFFFFFFFF);
```
Print a white X.

---

# Header files

---

## Renderer.h
Header for **Renderer.c**.
### Functions:
```c
void        printf          (const char* str, ...);
void        put_pixel       (point_T coords, color_t clr);
color_t     get_pixel       (point_T coords);
void        scroll          ();
```

---

## Color.h
This header defines **color_t** (a uint32_t typedef) and color codes.

### Standard Color Codes:
```c
//                          A R G B
enum ColorStandard_E {
    COLOR_STD_RED       = 0xffff0000,
    COLOR_STD_YELLOW    = 0xffffff00,
    COLOR_STD_CYAN      = 0xff00ffff,
    COLOR_STD_MAGENTA   = 0xffff00ff,
    COLOR_STD_BLACK     = 0xff000000,
    COLOR_STD_WHITE     = 0xffffffff
};
```


### Color Palette
These colors should be used for most cases, because they are designed to look nice together.
Colors from [Gogh](https://gogh-co.github.io/Gogh/).
```c
//                              A R G B
enum ColorPalette_E {
    COLOR_PAL_GREY_DARK     = 0xff363636,
    COLOR_PAL_PINK          = 0xffff0883,
    COLOR_PAL_GREEN_SIGNAL  = 0xff83ff08,
    COLOR_PAL_ORANGE        = 0xffff8308,
    COLOR_PAL_BLUE          = 0xff0883ff,
    COLOR_PAL_PURPLE        = 0xff8308ff,
    COLOR_PAL_GREEN         = 0xff08ff83,
    COLOR_PAL_GREY_LIGHT    = 0xffb6b6b6
};
```

---

## Font.h
This header contains the structs to handle PSF2 fonts.
Documentation for these can be found at the Bootloader documentation.

---

## Framebuffer.h
This header contains the struct for the framebuffer.
Documentation for this can be found at the Bootloader documentation.