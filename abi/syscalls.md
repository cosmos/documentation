# Syscalls

## Categories of syscalls:

- Files  
    CosmOS follows a modified form of the *NIX-philosophy,
    where not strictly everything is a file, but everything
    that makes sense to be made as a file is one.

- Modularity  
    This category contains syscalls that are needed
    to make exchangable kernel modules with an API
    that is standardized possible and easy to write.

- IPC (Inter-Process Communication)  
    In a highly modular kernel, good IPC is everything.

- Threads & Processes  
    To fully use the potential of modern multi-core -
    (and many-core -) systems, multithreading and multi-
    processing must be convenient.

- I/O - Access  
    The CosmOS-Kernel will NOT allow drivers to access
    the I/O directly; they need to pass requests through
    the kernel that then accepts or denies those.

- Security & Privileges  
    The CosmOS-system has a concept of security "rings".
    Rings are borders of security that, for passing through
    them, need a passphrase or similar to be given.
    Rings can be nested into each other, producing
    "sub-rings", known as a "splitter".



## Syscall Definitions

### Miscellaneous
```
NUM/ID  NAME            ARG 1           ARG 2           ARG 3
0x0001  exit            status
```


### Files
Files are identified through *file descriptors*, simple signed 32-bit
numbers which uniquely identify an opened file. If a file isn't used
currently, there is no file descriptor for it.

To get a file descriptor of a file from a path, that path must be given
to the syscall `fsopen`, and later closed with `fsclose`

There are some default file descriptors which are always inherited by
the parent process. These are STDIN (fd: 1), STDOUT (fd: 2), STDWARN (fd: 3)
and STDERR (fd: 4). These are by-default used to output/input text to/from
the text output device and text input device, be it a monitor & keyboard
or a simple log file.

```
NUM/ID  NAME          ARG 1         ARG 2         ARG_3         ARG 4
0x0101  fsopen        path          lenpath       usage
0x0102  fsclose       file
0x0103  fscreate      path          lenpath       usage
0x0104  fsdelete      file
0x0105  fsread        file          offset        buffer        lenbuffer
0x0106  fswrite       file          offset        data          lendata
0x0107  fsstat        file          attribute     buffer
0x0108  fslenstat     file          attribute
```

### Modularity
```
NUM/ID  NAME          ARG 1         ARG 2         ARG 3        ARG 4
0x0201  mod_load      type          path          len_path
0x0202  mod_loadstd   type
    Loads the default module of the type 'type'. That type can be
    anything from the list in Appendix B: Module Types.

0x0203  modunload     module
    Unloads the module that was previously loaded as 'module'.

0x0204  modgetsym     module        sym_name      lensymname
    Gets the symbol 'sym_name' from the module 'module'.

0x0205  modcreate     types         num_types     path          lenpath
    Tells the kernel to create the needed internal structures for a
    module that has the types that are in 'types' and that the SOs of
    are at the paths 'paths'.

0x0206 modconfig      module        field         value         lenvalue
    Sets a configuration value of a module before it is activated
    To see all the possible values for 'field' and what 'value' then
    should be, look at Appendix C: Module Configuration Fields.

0x0207  modcleanup    module
    Cleans up the resources that were allocated by the kernel module.

0x0208  modsetstd     module
    Sets the module 'module' as the standard of its type
```


## Appendix A: Datatypes
```
UINT            An unsigned integer type of the machine's wordsize
INT             A signed integer of type of the machine's wordsize
BOOL            Like an UINT, but 0 stands for FALSE and the rest for TRUE
ARRAY           A contiguous collection of data of one type
STRING          NOT NULL-terminated text
NULSTR          NULL-terminated text
STRUCTURE       A structure that is defined somewhere else
```



## Appendix B: Module Types
This list contains all the types of modules that can be interacted
with and created in the CosmOS-Kernel with their type identifier and
an explanation of what they do.

### General Drivers

0x0000  Unused
    A value representing NULL; nothing or EOF / End of Stream

0x0101  PCIe Driver
    This type of module provides raw PCI & PCIe connections.

0x0102  Generic PCIe Adapter
    This type of module gets the control over a PCIe device's connection
    if its description tells the kernel that it can handle the device type.


### Disk- and File-related drivers

0x0101  AHCI Driver
    A specialization of the Generic PCIe Adapter that is just for
    SATA-/ AHCI-usage.

0x0101  NVMe Driver
    A specialization of the Generic PCIe Adapter that is just for
    NVMe drive communication.

0x0101  Framebuffer Driver
    These modules create a framebuffer for a high-level graphics driver
    to use to render e.g. a TTY or similar.

0x0101  Filesystem Driver
    A driver for one or more specific filesystems. These drivers aren't
    visible to the user directly (and thus are low-level drivers), just
    the virtual filesystem is visible to the normal user.

0x0111  Network Driver
0x0111  Sound Driver



### Drivers with exposed API

0x0f11  Graphics Driver
    A high-level graphics driver that can implement functionality such
    as a Vulkan-interface or OpenGL rendering.

0x0f12  TI driver
    A driver for a Text-Interface.

0x0f7f  Emulation Driver

## Appendix C: Module Configuration Fields

